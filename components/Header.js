import { StyleSheet, Text, View } from 'react-native'
import React from 'react'


const Header = () => {
  return (
    <View style={styles.headerWrapper}>
      <Text style={styles.logo}>My Shopping List</Text>
    </View>
  )
}

export default Header

const styles = StyleSheet.create({
    headerWrapper: {
        backgroundColor: 'blue',
        justifyContent: 'center',
        alignItems: 'center',
        minHeight:30,
        paddingTop:50,
        paddingBottom:15,
    },
    logo: {
        fontSize: 30,
        fontWeight: 'bold',
        color: 'white',
        // fontFamily: 'inter-bold'
    }
})