import { StyleSheet, TextInput } from 'react-native'
import React from 'react'
// import Colors from '../constants/Colors'

const Input = (props) => {
  return (
    <TextInput 
        {...props}
        style={{...styles.textInput, ...props.style}}
        placeholder={props.textPlaceholder}
        onChangeText={ props.onChangeHandler }
        value={ props.inputValue }
    />
  )
}



const styles = StyleSheet.create({
    textInput: {
        borderColor: "cyan",
        borderWidth: 1,
        height: 50,
        marginVertical: 10,
        borderRadius: 30,
    }
})

export default Input