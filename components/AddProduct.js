import React, { useState } from 'react';
import { 
    StyleSheet, 
    TextInput, 
    Button,
    View, 
    Modal
} from 'react-native';
import ButtonComponent from './ButtonComponent';
import Input from './Input';

const AddProduct = ( { submitHandler, displayModal, cancelNewProduct } ) => {

    const [product, setProduct] = useState('');
    
    const inputHandler = (val) => { 
        // const intRegex = /^[0-9]+$/;
        const regex = /[^a-z]/gi;
        setProduct(val.replace(regex, ''));
    };

    const handlePress = () => {
        submitHandler(product);
        setProduct('');
    }

    return (
        <Modal
            visible={displayModal}
            animationType='slide'
        >
            <View style={styles.inputContainer}>
                {/* <TextInput
                    style={styles.textInput}
                    placeholder="Nouveau Produit"
                    onChangeText={ inputHandler }
                    value={ product }
                />  */}
                <Input 
                    style={styles.textInput}
                    textPlaceholder="Nouveau Produit"
                    onChangeHandler={ inputHandler }
                    inputValue={ product }
                    maxLength={ 10 } // ceci fonctionne le spread operator {...props} dans Input.js 
                />
                <View style={styles.btnContainer} >
                    <ButtonComponent
                        onPressHandler={ handlePress }
                        style={styles.btnBlue}
                    >
                        Ajouter
                    </ButtonComponent>
                    
                    <ButtonComponent 
                        onPressHandler={ cancelNewProduct }
                        style={styles.btnRed}
                    >
                        Annuler
                    </ButtonComponent>
                </View>
            </View>
        </Modal>
    )
}

const styles = StyleSheet.create({
    inputContainer: {
        flex: 1,
        // backgroundColor: 'yellow',
        marginBottom: 10,
        justifyContent: 'center',
        padding: 20,
    },
    textInput: {
        padding: 5,
        textAlign: 'center',
        paddingLeft: 9,
        fontSize: 18,
    //   flexGrow: 1,
    },
    btnContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '100%',
    },
    btnBlue: {
        backgroundColor: 'seagreen',
        width: 150
    },
    btnRed: {
        backgroundColor: 'tomato',
        width: 150
    },
});

export default AddProduct;
