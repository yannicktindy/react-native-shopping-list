import React, { useState } from 'react';
import { StyleSheet, Text, View, FlatList, Modal, Pressable, TextInput, Button, Image, ImageBackground,   } from 'react-native';
// import * as Font from 'expo-font';
// import AppLoading from 'expo-app-loading';
import Products from './components/Products';
import AddProduct from './components/AddProduct';
import DismissKeyboard from './components/DismissKeyboard';
import ButtonComponent from './components/ButtonComponent';
import Header from './components/Header';
import Colors from './constants/colors';

const fetchFonts = () => {
  return Font.loadAsync({
    'inter-bold': require('./assets/fonts/Inter-Bold.ttf'),
    'inter-regular': require('./assets/fonts/Inter-Regular.ttf'),
  })
}

export default function App() {

  const [myProducts, setMyProducts] = useState([])
  const [showModal, setShowModal] = useState(false)
  const [displayModal, setDisplayModal] = useState(false)
 
//   const [fontLoaded, setFontLoaded] = useState(false)
// if (!fontLoaded) {
//     return( 
//       <AppLoading 
//         startAsync={fetchFonts}
//         onFinish={() => setFontLoaded(true)}
//         onError={(err) => console.log(err) }
//       />
//     )
//   }

  const submitHandler = (product) => {
    setDisplayModal(false)
    if (product.length > 3) {
      const idString = Date.now().toString();
      setMyProducts( curentMyProducts => [{ key: idString, name: product }, ...curentMyProducts]) 
    } else {
      setShowModal(true)
    }
  }

  const deleteProduct = (key) => {
    setMyProducts( curentMyProducts => {
      return curentMyProducts.filter( product => product.key != key )
    })
  }

  const cancelNewProduct = () => {
    setDisplayModal(false)
  }

  return (
    <DismissKeyboard>
      <ImageBackground 
        style={styles.bgImage}
        source = {{ uri: 'https://cdn.pixabay.com/photo/2016/02/17/16/09/vertical-1205295_1280.jpg'}}
      >
      <Header />
      <View style={styles.container}>
        <Modal visible={showModal}
          onRequestClose={() => setShowModal(false)}
          transparent
        >
          <View style={styles.modalContainer}>
            <View style={styles.modalContent}>
              <View style={styles.modalHeader}>
                <Text style={styles.modalHeaderText}>Erreur</Text>
              </View>
              <View style={styles.modalBody}>
                <Image 
                  // source = {{ uri: 'https://www.pngitem.com/pimgs/m/146-1468479_warning-icon-png-red-exclamation-mark-transparent-png.png'}}
                  source={require('./assets/red-cross.png')}
                />
                <Text style={styles.modalBodyText}>Le produit doit contenir au moins 3 caractères</Text>
              </View>  
              <View style={styles.modalFooter}>
                <Pressable
                  style={styles.pressableBtnModal}
                  onPress={() => setShowModal(false)}
                  animatonType='slide'
                >
                  <Text style={styles.modalBtn}>OK</Text>
                </Pressable>
              </View>  
              
            </View>
          </View>

        </Modal>
        <ButtonComponent
          onPressHandler={() => setDisplayModal(true)}
          style={styles.addProductBtn}
        >
          Nouveau Produit
        </ButtonComponent>
       
        <AddProduct 
          submitHandler={ submitHandler } 
          displayModal = { displayModal } 
          cancelNewProduct = { cancelNewProduct} 
        />
        
        <FlatList 
          data={ myProducts } 
          renderItem={( { item } ) => 
            <Products 
              name={ item.name } 
              idString={ item.key }
              deleteProduct={ deleteProduct }
            />
          }
        />
      </View>
    </ImageBackground>
  </DismissKeyboard>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    paddingTop: 50,

  },  
  textInput: {
    borderColor: Colors.second,
    borderWidth: 1,
    fontSize: 20,
    marginBottom:10,
  },  
  modalContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,.5)',
  },
  modalContent: {
    backgroundColor: 'white',
    width: '80%',
    height: 300,
    borderRadius: 10,
    alignItems: 'center',
  },
  modalHeader: {
    width: '100%',
    padding: 10,
    alignItems: 'center',
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    borderBottomWidth: 1,
    borderBottomColor: 'grey',
  },
  modalHeaderText: {
    color: 'grey',
    fontSize: 20,
  },
  modalBody: {
    width: '100%',
    flex: 1,
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
  }, 
  modalBodyText: {
    color : 'red',
  },
  modalFooter: {
    width: '100%',
    padding: 10,
    alignItems: 'center',
    backgroundColor: 'lightgrey',
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
  },   
  pressableBtnModal: {  
    backgroundColor: 'red',
    borderRadius: 10,
  },
  modalBtn: {
    fontSize: 20,
    color: 'white',
    textAlign: 'center',
    padding: 10,

  },
  addProductBtn: {
    backgroundColor: Colors.primary,
    padding: 20,
    borderRadius: 30,
    marginBottom: 20,
  },
  bgImage: {
    flex: 1,
    resizeMode: 'cover',
  }
});
